import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RechargeComponent } from './recharge/recharge.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
{path: 'recargas', component: RechargeComponent, canActivate: [AuthGuard]},
{ path: 'login', component: LoginComponent },
{path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
