import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RechargeService {

  private apiUrl = 'http://137.184.230.141:5000/api/v1/enviar_recarga';

  constructor(private http: HttpClient) { }

  getData(operador_id: string, numero: string, valor: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const body = {
      operador_id: operador_id,
      numero: numero,
      valor: valor
    };

    return this.http.post<any>(this.apiUrl, body, { headers });
  }
}
