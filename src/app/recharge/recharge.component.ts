import { Component, OnInit } from '@angular/core';
import { OperadoresService } from '../operadores.service';
import { RechargeService } from '../recharge.service';

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss']
})
export class RechargeComponent implements OnInit{
  items: any[] = [];
  operador_id: string = '';
  numero: string = '';
  valor: string = '';
  alert  = false;
  alert2= false;
  message: string = '';

  constructor(private operadoresServices: OperadoresService, private rechargeServices: RechargeService) { }

  ngOnInit(): void {
    this.operadoresServices.getData().subscribe({
      next: (data) => {
        this.items = data?.data;
      },
      error: (err) => console.error('Error fetching data: ', err)
    });
  }

  rechargeMobile(): void {
    this.rechargeServices.getData(this.operador_id, this.numero, this.valor).subscribe({
      next: (response) => {
        this.alert= true;
        this.alert2= false;
        this.message= response?.message;
        this.resetForm();
      },
      error: (error) => {
        this.alert2= true;
        this.alert= false;
      }
    });
  }
    private resetForm() {
      this.operador_id = '';
      this.numero = '';
      this.valor = '';
    };


}
